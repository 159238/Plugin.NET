﻿namespace PluginNET.error
{
    /// <summary>
    /// 加载插件时发生错误的错误类型
    /// </summary>
    public enum PluginErrorTypes
    {
        /// <summary>
        /// 没有错误
        /// </summary>
        None,

        /// <summary>
        /// 插件不是有效的托管dll文件
        /// </summary>
        InvalidManagedDllFile,

        /// <summary>
        /// 无法从程序集中加载类的类型定义，这可能是dll依赖的文件不存在引起的
        /// </summary>
        CannotLoadClassTypes,

        /// <summary>
        /// 插件内未找到实现指定接口的类
        /// </summary>
        ImplementionClassNotFound,

        /// <summary>
        /// 不合法的类型定义，这可能是类型不是class，修饰符包含 abstract 或 未声明为 public
        /// </summary>
        IllegalClassDefinition,

        // 插件的类不包含无参构造函数
        ZeroParameterConstructorNotFound,

        /// <summary>
        /// 实例创建失败
        /// </summary>
        InstanceCreateFailed,

        /// <summary>
        /// 未知错误
        /// </summary>
        Unkown,
    }
}
