﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginNET
{
    /// <summary>
    /// 插件信息
    /// </summary>
    [Serializable]
    public class PluginInfo
    {
        /// <summary>
        /// 完整文件名
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 是否支持热卸载（继承了MarshalByRefObject）
        /// </summary>
        public bool HotUnloading { get; set; }

    }
}
